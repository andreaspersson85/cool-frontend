import { AndreascoolstuffPage } from './app.po';

describe('andreascoolstuff App', () => {
  let page: AndreascoolstuffPage;

  beforeEach(() => {
    page = new AndreascoolstuffPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
