import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule }   from '@angular/forms';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { FlexLayoutModule } from "@angular/flex-layout";
import { HttpModule }    from '@angular/http';

import { AppComponent }        from './app.component';
import { HttpService } from './http.service';

@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
    BrowserAnimationsModule,
    FlexLayoutModule,
    HttpModule
   ],
  declarations: [
    AppComponent,       
  ],
  bootstrap: [ AppComponent ]
})

export class AppModule { }
