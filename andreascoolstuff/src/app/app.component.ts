import { Component } from '@angular/core';

import { Article } from './article';
import { HttpService } from './http.service';

const ARTICLES: Article[] = [
  { nbr: 1, name: 'Ice Cube Swe', image: '/assets/petter.jpg', article_id: 102 },
  { nbr: 2, name: 'Powerball', image: '/assets/zlatan.jpg', article_id: 88 },
];

@Component({
  selector: 'app-component',
  providers: [HttpService],
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent {
  constructor(private httpService: HttpService) {}
  cartToken = {token: '', country_code: ''};  
  articleArray = ARTICLES;
  selectedArticle: Article;
  articles = { 
    "articles": [{
    "article_id": Number,
    "article_quantity": Number, 
    }], 
    "language_code": "de",
    "currency_code": "EUR",
    "country_code":"de" 
  }
  tmp = [];
  cartResponse = {};

  getCartToken(){ 
    this.httpService.getToken().subscribe(data => this.cartToken = data, 
    error => console.log('oops, error ' + error),
    () => this.setLocalStorage(this.cartToken.token));  
  }    
  
  setLocalStorage = (string) => window.localStorage.setItem('0', string);

  getLocalStorage = (key) => window.localStorage.getItem(key); 
  
  addToCart(article){
    this.tmp = [];
    this.tmp.push({"article_id": article.article_id, "article_quantity": 1})
    this.articles.articles = (this.tmp)
    
    this.httpService.addToCart(this.getLocalStorage('0'), this.articles).subscribe(data => this.cartResponse = data,
    error => console.log('oops, error ' + error),
    () => console.log(this.cartResponse)); //Only to demonstrate
    console.log(this.cartToken.token)      //Only to demonstrate
    
    document.getElementById("fadeInAndOut").innerText = "+1";
    document.getElementById("fadeInAndOut").className = "elementToFadeInAndOut";
    
    setTimeout(() => {
        document.getElementById("fadeInAndOut").className = "";
        document.getElementById("fadeInAndOut").innerText = "";
      }, 4000) 
    }

    ngOnInit(){
     this.getCartToken();
    }
}
